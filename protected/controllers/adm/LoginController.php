<?php

class LoginController extends Controller
{
	public function actionIndex()
	{
		$this->layout='main_login';
		$this->render('login');		
		
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}