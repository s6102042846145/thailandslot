<?php
	$this->pageTitle = 'โปรโมชั่น' . Yii::app()->params['prg_ctrl']['pagetitle'];	
?>
<div class="main-body">
	<div class="page-wrapper">
		<!-- Page-body start -->
		<div class="page-body">
			<div class="row">
				<!-- Material statustic card start -->
				<div class="col-md-12">
					<div class="card mat-stat-card">
						<div class="card-block ">
							<div class="text-center padding20" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
								<img src="image/title/Promotion.png" class="topic-page-first" />
							</div>
							<div class="row">
								<div class="col-md-6 box-promotion">
									<div class="text-right padding20" data-aos="fade-down-left">
										<img src="image/promotion/สมาชิกใหม่.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-left padding20" data-aos="fade-down-right">
										<img src="image/promotion/ฝาก500 7 วัน.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-right padding20" data-aos="fade-down-left">
										<img src="image/promotion/นาทีทอง.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-left padding20" data-aos="fade-down-right">
										<img src="image/promotion/ชวนเพื่อน.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-right padding20" data-aos="fade-down-left">
										<img src="image/promotion/คืนยอดเสีย.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-left padding20" data-aos="fade-down-right">
										<img src="image/promotion/ครั้งแรกของวัน.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-right padding20" data-aos="fade-down-left">
										<img src="image/promotion/กงล้อพาราย.png" class="img370 border-gold" />
									</div>
								</div>
								<div class="col-md-6 box-promotion">
									<div class="text-left padding20" data-aos="fade-down-right">
										<img src="image/promotion/5บิล.png" class="img370 border-gold" />
									</div>
								</div>
							</div>
							<div class="text-center padding20" data-aos="fade-up">
								<img src="image/รวม.png" class="img370">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Page-body end -->
	</div>
	<div id="styleSelector"> </div>
</div>

<div class="monkey-king">
		<div class="flex-mt">
			<img src="image/monkey-bottom.png" class="mk">
		</div>
	</div>
	<div class="footer-not-fixed">
		<div class="content-footer flex-footer">
			<div class="text-center">
				<img src="image/ป้ายซ้าย.png" class="img-flud">
			</div>
			<div class="text-center">
				<div class="group-cont">
					
					<img src="image/slot xo.png" class="topic-footer">
					<p class="p-cont"><b style="font-size: 16px; font-weight: bold;">THAILAND SLOT </b>เราคือผู้ให้บริการสล็อตออนไลน์ <br>
						บนมือถืออันดับ 1 รองรับทั้ง IOS และ Android <br>
						ไม่ต้องดาวน์โหลดให้เปลืองเน็ต มีเกมส์ให้เลือกเล่น <br>
						มากมายหลายเกมส์ เติมเงิน ถอนเงิน <br>
						ผ่านระบบอัตโนมัติ มั่นคง ปลอดภัย บริการตลอด 24 ช.ม</p>
					<img src="image/2.png" class="slot">
				</div>
			</div>
			<div class="text-center">
				<div class="">
					
					<img src="image/เข้าเกม.png" class="topic-footer">
					<div class="regis">
						<a href="https://bit.ly/3yr8GoG"><img src="image/ป้ายเข้าสู่ระบบ.png" class="hovOpa img-footer login"></a>
						<a href="https://bit.ly/2ReRYrT"><img src="image/ป้ายสมัคร.png" class="hovOpa img-footer register"></a>
					</div>
					<img src="image/ดาวโหลด.png" class="topic-footer">
					<div class="regis">
						<img src="image/AppStore.png" class="img-footer">
						<img src="image/Google.png" class="img-footer">
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="footer-copy" style="background-image:url(image/แถบล่าง.png);">
		<p>2021 @ COPYRIGHT THAILANDSLOT.COM ALL RIGHTS RESERVED</p>
	</div>

	<!-- menu fixed -->
	<div class="fixed-left">
		<div class="inner-fixed">
			<div class="welcome" href="home">
				<img class="hovOpa" id="close" src="image/menu/close.png" style="width: 50px;position: relative;top: 50px;left: 100px;">
				<img src="image/menu/Cartoon.png" class="moblie-menu-footer-side" style="width:240px">
				<img src="image/menu/ยินดีต้อยรับ.png" class="moblie-menu-footer-d">
				<img src="image/menu/สมาชิกทุกท่าน.png" class="moblie-menu-footer-d">
			</div> 
			<a href="Register"><img src="image/menu/ป้าย สมัคร สมาชิก.png" class="hovOpa" style="width:170px;"></a>
			<a href="https://bit.ly/2RWMe66"><img src="image/menu/ป้าย line.png" class="hovOpa moblie-menu-footer-side"></a>
			<a href="https://bit.ly/3hs6O97"><img src="image/menu/ป้าย Facebook.png" class="hovOpa moblie-menu-footer-side"></a>
			<a href="#"><img src="image/Top/ป้าย กงล้อพารวย.png" class="hovOpa" style="width:165px;"></a>
		</div>
	</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#promotion").addClass("active");
		
	});
</script>
	