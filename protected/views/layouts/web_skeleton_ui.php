<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />

		<meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
		<meta name="author" content="Codedthemes" />
		<!-- Favicon icon -->
		<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/images/favicon.ico" type="image/x-icon">
		<!-- Google font-->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
		<!-- waves.css -->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
		<!-- Required Fremwork -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/bootstrap/css/bootstrap.min.css">
		<!-- waves.css -->
		<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/pages/waves/css/waves.min.css" type="text/css" media="all">
		<!-- themify icon -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/icon/themify-icons/themify-icons.css">
		<!-- font-awesome-n -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/font-awesome-n.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/font-awesome.min.css">
		<!-- scrollbar.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/jquery.mCustomScrollbar.css">
		<link rel="stylesheet" type="text/javascript" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/burger.js">
		<!-- Style.css -->
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/app.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/rules.css">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/css/game.css">
		
		<!-- slider game page using bootstrap-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- font thai -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">

        <!-- aos animate -->
        <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css" />
		
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>  

	</head>
    <script>
        $(document).ready(function() {
        $(window).on("scroll", function() {
            if ($(window).scrollTop() >= 20) {
            $(".navbar-top").addClass("compressed");
            $("#change-responsive").addClass("responsive-bg");
            $("#change-responsive").removeClass("bg");
            $("#logo-responsive").addClass("img-logo-2");
            $("#ul-responsive").addClass("ul-mt");
            } else {
            $(".navbar-top").removeClass("compressed");
            $("#change-responsive").removeClass("responsive-bg");
            $("#change-responsive").addClass("bg");
            $("#logo-responsive").removeClass("img-logo-2");
            $("#ul-responsive").removeClass("ul-mt");
            }
        });
        });
    </script>
	<body>
	<!-- Pre-loader start -->
    
	<!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="desktop navbar header-navbar pcoded-header">
                <div class="navbar-top" style="background-image:url(image/แถบล่าง.png);margin-left: -1px;">
                    <div class="left-bar">
                        <a  href="https://bit.ly/2RWMe66"><img src="image/line icon.png" style="width:45px; margin-bottom: 2px;"></a>
                        <a  href="https://bit.ly/3hs6O97"><img src="image/face icon.png" style="width:50px;"></a>
                        <a class="menubar-a-left" href="">@ThailandSlot</a>
                        <a class="menubar-a-left-last" href="">สล็อตออนไลน์</a>
                    </div>

                    <div class="right-bar">
                        <a href="http://www.slotxo.com/mobile" class="menubar-a" href="">ดาวน์โหลด</a>
                        <a href="promotion" class="menubar-a" href="">โปรโมชั่น</a>
                        <a href="rules" class="menubar-a" href="">กฎและกติกา</a>
                        <a href="" class="menubar-a-last" href="">ติดต่อเรา</a>
                    </div>
                </div>

                <div class="header-navbar pcoded-header">
                    <div class="menu-top sp-desktop"> 
                        <ul class="nav-left">
                            <li>
                                <a class="mobile-menu waves-effect waves-light"  href="/home" data-aos="zoom-out-left">
                                    <img class="hovOpa slot-logo" src="image/Top/logothslot.png" />
                                </a>
                            </li>
                        </ul>

                        <div class="nav-center">
                            <img id="logo-responsive" src="image/logo.png" class="img-logo">
                        </div>

                        <ul class="nav-right">
                            <li class="">
                                <a href="Login" class="waves-effect waves-light" data-aos="zoom-out-right">
                                    <img class="hovOpa top-bar" src="image/ป้ายเข้าสู่ระบบ.png" />
                                </a>
                            </li>
                            <li class="">
                                <a href="Register" class="waves-effect waves-light"  data-aos="zoom-out-right">
                                    <img class="hovOpa top-bar" src="image/ป้าย สมัครสมาชิก.png" />
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="navbar-wrapper bg-mt">
                        <div class="navbar-logo">
			                <a class="mobile-show waves-effect waves-light" id="mobile-collapse" style="position: absolute;z-index: 999;left: 0;">
                                <div id="addClass" class="burger-none">&nbsp</div>
                            </a>
                                                            
                            <script>
                                    $(document).ready(function(){
                                        $("#mobile-collapse").click(function(){
                                            $("#addClass").toggleClass("burger-closed");
                                        });
                                    });

                                    
                            </script>
                            <div class="mobile">
                                <img  src="image/logo.png" class="img-logo">
                            </div>
                            <a href="https://bit.ly/3hs6O97" class="mobile-options waves-effect waves-light" style="margin-right: 45px;">
                                <img class="hovOpa" src="image/face icon.png" style="width: 50px;" />
                            </a>
                            <a href="https://bit.ly/2RWMe66" class="mobile-options waves-effect waves-light">
                                <img class="hovOpa" src="image/line icon.png" style="width: 45px;" />
                            </a>
                        </div>
                            

                    </div>
                </div>

                <!-- Navbar new -->
                <div class="navbar-bottom" >
                    <ul id="ul-responsive" class="menu-bottom" style="margin-top: 100px;">
                        <li class="menu-bar">
                            <a class="menu-bar" href="home">
                                <img id="login" class="hovOpa" src="image/ป้าย หน้าหลัก.png" alt="">
                            </a>
                        </li>
                        <li class="menu-bar">
                            <a  class="menu-bar"href="promotion">
                                <img id="promotion" class="hovOpa" src="image/Top/ป้าย โปรโมชั่น.png " alt="">
                            </a>
                        </li>
                        <li class="menu-bar">
                            <a  class="menu-bar"href="rules">
                                <img class="hovOpa rules" src="image/Top/rules.png" alt="">

                            </a>
                        </li>
                        <li  class="menu-bar">
                            <a class="menu-bar" href="">
                                <img id="contact" class="hovOpa" src="image/Top/contact.png">

                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end navbar new -->
            </nav>
	   
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
		            <nav class="nav-none pcoded-navbar">
                        <div class="sidebar_toggle">
                            <a href="#">
                                <i class="icon-close icons"></i>
                            </a>
                        </div>
                        <div class="pcoded-inner-navbar main-menu" style="background-image:url('image/bg_menu.png')">
                            <ul class="pcoded-item pcoded-left-item">
								<li id="home" class="mt-5">
                                    <a href="home">หน้าแรก</a>
                                </li>
								<li id="register" class="">
                                    <a href="https://bit.ly/2ReRYrT">สมัครสมาชิก</a>
                                </li>
								<li id="login" class="">
                                    <a href="https://bit.ly/3yr8GoG">เข้าสู่ระบบ</a>
                                </li>
								<li id="rules" class="">
                                    <a href="rules" style="display: flex;">กฏ - กติกา <div class="warn"><span>อ่าน</span></div></a>
                                </li>
								<!--li id="deposit" class="">
                                    <a href="">ฝาก - ถอน</a>
                                </li-->
								<li id="download" class="">
                                    <a href="http://www.slotxo.com/mobile">ดาวน์โหลด</a>
                                </li>
								<li id="promotion" class="">
                                    <a href="promotion">โปรโมชั่น</a>
                                </li>
							</ul>                          
                        </div>
                    </nav>	
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <!-- navbar-new  mobile-->
                            <div class="navbar-new mobile">
                                <ul class="mt-4 nav-bar">
                                    <li class="menu-bar">
                                        <a href="rules" class="hovOpa">
                                            <img class="hovOpa rules Mobi" src="image/Top/rules.png">

                                        </a>
                                    </li>
                                    <li class="menu-bar">
                                        <a class="hovOpa" href="">
                                            <img class="hovOpa Mobi" src="image/Top/ป้าย กงล้อพารวย.png">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end navbar new -->	
								<?php echo $content; ?> 

                        </div>
                    </div>
                </div>
            </div>

            <!-- footer -->
            <div class="wrap-footer mobile-show" >
                <div class="footer-inner">
                    <ul class="menu-footer">
                        <li>
                            <a href="login" style="position: relative;bottom: 40px;"><img src="image/ป้าย.png" style="width:120px;"></a>
                        </li>
                    </ul>
                </div>

                <div class="footer-inner">
                    <ul class="menu-footer">
                        <li class="flex-row">
                            <a href="home"><img src="image/หน้าหลัก.png" class="moblie-menu-footer"></a>
                            <a href="Register"><img src="image/สมัคร.png" class="moblie-menu-footer"></a>
                            <a href="promotion"><img src="image/โปรโม.png" class="moblie-menu-footer"></a>
                            <a href="#"><img src="image/ติดต่อเรา.png" class="moblie-menu-footer"></a>
                        </li>   
                    </ul>
                </div>
            </div>
			<!-- end footer -->
        </div>
    </div>

    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery/jquery.min.js "></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery-ui/jquery-ui.min.js "></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/popper.js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/bootstrap/js/bootstrap.min.js "></script>
    <!-- waves js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/pages/waves/js/waves.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- slimscroll js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/jquery.mCustomScrollbar.concat.min.js "></script>

    <!-- menu js -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/pcoded.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/vertical/vertical-layout.min.js "></script>

    <!-- js aos animate -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.6/highlight.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/assets/js/script.js "></script>
	
    <script type="text/javascript">
		$(document).ready(function(){
			$(".fixed-button").addClass("d-none");
			$(".login").click(function(){
			  window.location.assign("/login");
			});
			$(".register").click(function(){
			  window.location.assign("/register");
			});
			$(".facebook").click(function(){
			  window.location.assign("https://www.facebook.com/");
			});
		});

        //close tab
        $("#close").click(function(){
            $(".fixed-left").hide();
        })
        
        //animation css
        AOS.init({
            easing: 'ease-out-back',
            duration: 1000
        });
		
	</script>
</body>

</html>



       



