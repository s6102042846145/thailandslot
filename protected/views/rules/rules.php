<?php
	$this->pageTitle = 'กฎและกติกา' . Yii::app()->params['prg_ctrl']['pagetitle'];
	
?>

                            <!-- Main-body start -->
                                <div class="main-body">
                                    <div class="page-wrapper">
                                        <!-- Page-body start -->
                                        <div class="page-body" style="margin-top: 100px;">
                                            <div class="row">
                                                <!-- Material statustic card start -->
                                                <div class="col-md-12">
                                                    <div class="card mat-stat-card">
                                                        <div class="card-block padding20">
                                                            <div class="text-center" data-aos="fade-down">
                                                                <img src="image/rules/topics.png" class="topic-page" />
                                                            </div>
                                                            <div class="text-center padding20" data-aos="fade-up">
                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว1.png" class="rule-img">
                                                                    <img id="t1" src="image/rules/ป้ายเงื่อนไขการสมัคร.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_01" src="image/rules/รายละเอียดเงื่อนไขการสมัคร.png" class="parag" style="display:none;">
                                                                
                                                                <div class="rule-menu">
                                                                    <img   src="image/rules/ป้ายหัว2.png" class="rule-img">
                                                                    <img id="t2" src="image/rules/ป้ายการฝากเงิน.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_02" src="image/rules/รายละเอียดการฝากเงิน.png" class="parag" style="display:none;">
                                                                
                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว2.png" class="rule-img">
                                                                    <img  id="t3" src="image/rules/ป้ายกฏการฝากทรูวอลเลท.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_03" src="image/rules/รายละเอียดกฏการฝากทรูวอลเลท.png" class="parag" style="display:none;">


                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว2.png" class="rule-img">
                                                                    <img  id="t4" src="image/rules/ป้ายการฝากเงินทศนิยม.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_04" src="image/rules/รายละเอียดการฝากเงินทศนิยม.png" class="parag" style="display:none;">
                                                                
                                                                <div class="rule-menu">
                                                                    <img  src="image/rules/ป้ายหัว3.png" class="rule-img">
                                                                    <img  id="t5"src="image/rules/ป้ายการถอนเงิน.png" class="honNoneOpa rule-img-topic" />
                                                                </div>
                                                                <img id="terms_05" src="image/rules/รายละเอียดการถอนเงิน.png" class="parag" style="display:none;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Page-body end -->
                                    </div>
                                    <div id="styleSelector"> </div>
                                </div>
                                <div class="monkey-king">
                                    <div class="flex-mt">
                                        <img src="image/monkey-bottom.png" class="mk">
                                    </div>
                                </div>
                                <div class="footer-not-fixed">
                                    <div class="content-footer flex-footer">
                                        <div class="text-center">
                                            <img src="image/ป้ายซ้าย.png" class="img-flud">
                                        </div>
                                        <div class="text-center">
                                            <div class="group-cont">
                                                
                                                <img src="image/slot xo.png" class="topic-footer">
                                                <p class="p-cont"><b style="font-size: 16px; font-weight: bold;">THAILAND SLOT </b>เราคือผู้ให้บริการสล็อตออนไลน์ <br>
                                                    บนมือถืออันดับ 1 รองรับทั้ง IOS และ Android <br>
                                                    ไม่ต้องดาวน์โหลดให้เปลืองเน็ต มีเกมส์ให้เลือกเล่น <br>
                                                    มากมายหลายเกมส์ เติมเงิน ถอนเงิน <br>
                                                    ผ่านระบบอัตโนมัติ มั่นคง ปลอดภัย บริการตลอด 24 ช.ม</p>
                                                <img src="image/2.png" class="slot">
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <div class="">
                                                
                                                <img src="image/เข้าเกม.png" class="topic-footer">
                                                <div class="regis">
                                                    <a href="https://bit.ly/3yr8GoG"><img src="image/ป้ายเข้าสู่ระบบ.png" class="hovOpa img-footer login"></a>
                                                    <a href="https://bit.ly/2ReRYrT"><img src="image/ป้ายสมัคร.png" class="hovOpa img-footer register"></a>
                                                </div>
                                                <img src="image/ดาวโหลด.png" class="topic-footer">
                                                <div class="regis">
                                                    <img src="image/AppStore.png" class="img-footer">
                                                    <img src="image/Google.png" class="img-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer-copy" style="background-image:url(image/แถบล่าง.png);">
                                    <p>2021 @ COPYRIGHT THAILANDSLOT.COM ALL RIGHTS RESERVED</p>
                                </div>

                            <!-- menu fixed -->
                                <div class="fixed-left">
                                    <div class="inner-fixed">
                                        <div class="welcome" href="home">
                                            <img class="hovOpa" id="close" src="image/menu/close.png" style="width: 50px;position: relative;top: 50px;left: 100px;">
                                            <img src="image/menu/Cartoon.png" class="moblie-menu-footer-side" style="width:240px">
                                            <img src="image/menu/ยินดีต้อยรับ.png" class="moblie-menu-footer-d">
                                            <img src="image/menu/สมาชิกทุกท่าน.png" class="moblie-menu-footer-d">
                                        </div> 
                                        <a href="Register"><img src="image/menu/ป้าย สมัคร สมาชิก.png" class="hovOpa" style="width:170px;"></a>
                                        <a href="https://bit.ly/2RWMe66"><img src="image/menu/ป้าย line.png" class="hovOpa moblie-menu-footer-side"></a>
                                        <a href="https://bit.ly/3hs6O97"><img src="image/menu/ป้าย Facebook.png" class="hovOpa moblie-menu-footer-side"></a>
                                        <a href="#"><img src="image/Top/ป้าย กงล้อพารวย.png" class="hovOpa" style="width:165px;"></a>
                                    </div>
                                </div>
<script type="text/javascript">
		$(document).ready(function(){
			$("#rules").addClass("active");

		});

        //เปิด slider terms of register
        $(document).ready(function(){
            $("#t1").click(function(){
                $("#terms_01").toggle();
            })

            $("#t2").click(function(){
                $("#terms_02").toggle();
            })

            $("#t3").click(function(){
                $("#terms_03").toggle();
            })

            $("#t4").click(function(){
                $("#terms_04").toggle();
            })

            $("#t5").click(function(){
                $("#terms_05").toggle();
            })
        })
</script>