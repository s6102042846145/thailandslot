<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>"Thailand Slot",
	'language' => 'en',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('127.0.0.1','::1'),
			'ipFilters'=>array('*.*.*.*','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class' => 'CustomWebUser',
		),
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				//'<username:[\w.]+>'=>'user/post',
                //'<username:[\w.]+>/display'=>'image/display',			
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			'showScriptName'=>false,
		),

		// uncomment the following to use a MySQL database
		'db'=>array(
			
			'connectionString' => 'mysql:host=localhost;dbname=slot',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '1234',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

        'request'=>array(
            'enableCookieValidation'=>true,
			'enableCsrfValidation'=>true,
			//'baseUrl' => 'http://www.example.com',
        ),

		'clientScript' => array(
			'scriptMap' => array(
				'jquery.js' => false,
				'jquery.min.js' => false,
			),
		),		
		
		'CommonFnc' => array(
			'class'=>'CommonFnc',
        ),	
			
		'session' => array(
			'class' => 'CDbHttpSession',
			'timeout' => 60*20, //20นาที
		),
	),
 
	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	
	'params'=>array(
		'prg_ctrl'=>array(
  		    'domain' => 'ThailandSlot',  //eg. for set cookie
	        'indextitle' => "Thailand Slot",
			'pagetitle'	=> " | Thailand Slot",
			'logo' => 'ThailandSlot/images/logo.png',
			'noimg' => 'ThailandSlot/images/no-image.png',
			'img_cover' => 'ThailandSlot/images/cover_4.jpg',
			'img_cover_blur' => 'ThailandSlot/images/cover_4_blur.jpg',
			'daterange' => '1940:2016',
			'authCookieDuration' => 7,  //the duration of the user login cookie in days			
			'diffsvtime' => 0, //เวลาบนเครื่อง webserver ห่างจาก dbserver เท่าไหร่ เช่น 7 หมายถึง webserver ช้ากว่า dbserver 7 ชม			
			'url' => array(
				'baseurl' => 'ThailandSlot/', 
				'upload' => 'ThailandSlot/uploads',											
				'media' => 'ThailandSlot/media', 												
				),
			'path' => array(
				'basepath' => 'ThailandSlot/', 				
				'upload' => 'ThailandSlot/uploads', 												
				'media' => 'ThailandSlot/media', 
				'closepath' => '/', 
				/* D:\Project\ab_new\uploads
				'basepath' => '/127.0.0.1/karuphan_new/', 				
				'uploads' => '/127.0.0.1/karuphan_new/uploads/', 								
				'media' => '/127.0.0.1/karuphan_new/media/', 		
				*/															
			),		
			'vendor' => array(
				'phpthumb' => array( 		
					'path' => '/vendor/phpthumb/PhpThumbFactory.php',  	
					'ThumbLib' => '/protected/vendor/phpthumb/ThumbLib.inc.php',  	
													
				),	
				'jquery-upload' => array(
					'path' => '/vendor/jquery-upload/UploadHandler.php',				
					'path_intro' => '/vendor/jquery-upload/IntroUploadHandle.php',
					'path_import' => '/vendor/jquery-upload/ImportUploadHandle.php',	
					'path_convert' => '/vendor/jquery-upload/ConvertUploadHandle.php',						
				),
				'tcpdf' => array(
					 'oripath' => '/vendor/tcpdf/tcpdf.php', 
					 'path' => '/vendor/tcpdf/customtcpdf.php', 
					 'confpath' => '/vendor/tcpdf/config/tcpdf_config.php', 
				
				),	
				'phpexcel' => array(
					'path' => '/vendor/Classes/PHPExcel.php',
				),
				'iofactory' => array(
					'path' => '/vendor/Classes/PHPExcel/IOFactory.php',
				),				
			),	
			'pagination' => array(
				'default' => array ( 
					'pagesize' => '40',
					'maxbuttoncount' => '12',
					'maxitem' => '1000',									
				),
			),	 					
		),			 
			

	),
);